--[[

Platformer Engine
Copyright (C) 2013 Robert Cochran

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 3
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License in the LICENSE file for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

]]

modules = {};

modules.player = dofile("../src/player.lua");

-- Test creation

io.write("Creating a player object... ");

local myPlayer = modules.player:new();

if not myPlayer then
	error("Failed to create player object!!", 2);
end

io.write("OK\n");

-- Test set/get of X/Y

io.write("Testing get/set of X/Y... ");

myPlayer:setX(32);
myPlayer:setY(64);

if myPlayer:getX() ~= 32 then 
	error("myPlayer:getX() returned "..myPlayer:getX()..", NOT 32!", 2);
elseif myPlayer:getY() ~= 64 then
	error("myPlayer:getY() returned "..myPlayer:getY()..", NOT 64!", 2);
end

io.write("OK\n");

-- Test size

io.write("Testing get/set of size... ");

myPlayer:setSize(16);

if myPlayer:getSize() ~= 16 then
	error("myPlayer:getSize() returned "
		..myPlayer:getSize()..", NOT 16!", 2);
end

io.write("OK\n");

-- onGround test

io.write("Testing onGround bool... ");

myPlayer:setOnGround(true);

if not myPlayer:isOnGround() then
	error("myPlayer:isOnGround() returned "
		..myPlayer:isOnGround()..", NOT true!", 2);
end

io.write("OK\n");

-- XVel test

io.write("Testing get/setXVel... ");

myPlayer:setXVel(24);

if myPlayer:getXVel() ~= 24 then
	error("myPlayer:getXVel() returned "
		..myPlayer:getXVel()..", NOT 24!", 2);
end

io.write("OK\n");

-- YVel test

io.write("Testing get/setXYel... ");

myPlayer:setYVel(48);

if myPlayer:getYVel() ~= 48 then
	error("myPlayer:getYVel() returned "
		..myPlayer:getYVel()..", NOT 48!", 2);
end

io.write("OK\n");

-- Vertex test

io.write("Testing vertex returns... ");

local baseVerts = {
	myPlayer:getX(), myPlayer:getY();
	myPlayer:getX() + myPlayer:getSize(), myPlayer:getY();
	myPlayer:getX() + myPlayer:getSize(), 
		myPlayer:getY() + myPlayer:getSize();
	myPlayer:getX(), myPlayer:getY() + myPlayer:getSize();
};

local playerVerts = myPlayer:getVerts();

for i = 1, 8 do
	if baseVerts[i] ~= playerVerts[i] then
		error("playerVerts["..i.."] is "
			..playerVerts[i]..", NOT "..baseVerts[i], 2);
	end
end

io.write("OK\n");
